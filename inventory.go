package coffee_machine

import (
	"fmt"
	"sync"
)

type Inventory struct {
	// The base ingredients provided to the coffee machine are
	// stored in this map. All values in the map are in millilitres
	items map[string]int

	// Specifies beverages, ingredients and quantities required to prepare them
	recipes map[string]map[string]int

	// Used to ensure synchronised access to the inventory
	mu *sync.Mutex
}

func newInventory(config *Config) *Inventory {
	return &Inventory{
		config.Machine.TotalItemsQuantity,
		config.Machine.Beverages,
		&sync.Mutex{},
	}
}

func (inv *Inventory) TakeIngredients(beverage string, recipe map[string]int) error {
	// Acquire lock to the inventory
	inv.mu.Lock()
	// Schedule the lock to be release after this method returns
	defer inv.mu.Unlock()

	// Check if all the ingredients are available
	for ingredient := range recipe {
		_, ok := inv.items[ingredient]
		if !ok {
			return fmt.Errorf("%s is not available", ingredient)
		}
	}

	// Check if all the ingredients are present in required quantities
	for ingredient, requiredQuantity := range recipe {
		availableQuantity := inv.items[ingredient]
		if availableQuantity < requiredQuantity {
			return fmt.Errorf("item %s is %d", ingredient, availableQuantity)
		}
	}

	// Take ingredients to make beverage and set remaining quantities correctly
	for ingredient, requiredQuantity := range recipe {
		availableQuantity := inv.items[ingredient]
		inv.items[ingredient] = availableQuantity - requiredQuantity

		debug("ingredient %s is now %d = %d - %d",
			ingredient, inv.items[ingredient], availableQuantity, requiredQuantity)
	}

	debug("\n%s done: %+v", beverage, inv.items)

	return nil
}

// Fills inventory with a specific ingredient, if the ingredient is already present
// it updates that ingredient's quantity correctly
func (inv *Inventory) Fill(item string, quantity int) {
	// Acquire lock to the inventory
	inv.mu.Lock()
	// Schedule the lock to be release after this method returns
	defer inv.mu.Unlock()

	available, ok := inv.items[item]
	if !ok {
		inv.items[item] = quantity
	}

	inv.items[item] = available + quantity

}
