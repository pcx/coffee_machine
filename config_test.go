package coffee_machine_test

import (
	"testing"

	machine "pcx.io/coffee_machine"
)

func TestLoadConfig(t *testing.T) {
	config, err := machine.LoadConfig("./test_config.json")

	if err != nil {
		t.Errorf("Loading config failed with error: %v", err)
		return
	}

	if config.Machine.Outlets.Count != 3 {
		t.Errorf("Failed reading outlets count")
		return
	}

	hotWaterQuantity, ok := config.Machine.TotalItemsQuantity["hot_water"]
	if !ok || hotWaterQuantity != 500 {
		t.Errorf("Failed reading total items quantity")
		return
	}

	hotTeaBeverage, ok := config.Machine.Beverages["hot_tea"]
	if !ok {
		t.Errorf("Failed reading hot tea beverage")
		return
	}

	hotWaterQuantity, ok = hotTeaBeverage["hot_water"]
	if !ok || hotWaterQuantity != 200 {
		t.Errorf("Failed reading hot water quantity in hot tea beverage")
		return
	}
}
