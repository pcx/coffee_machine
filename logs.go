package coffee_machine

import (
	"fmt"
	"os"
)

// A simple wrapper to print well formatted debug messages.
// Set the env var 'DEBUG' to 'true' to enable them.
func debug(msg string, args ...interface{}) {
	if os.Getenv("DEBUG") == "true" {
		fmt.Printf(
			fmt.Sprintf("DEBUG: %s\n", msg),
			args...,
		)
	}
}
