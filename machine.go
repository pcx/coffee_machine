package coffee_machine

import (
	"fmt"
	"sync"
	"time"
)

const (
	PrepTimeInMillis = 100
)

// Verifies if all required ingredients for a beverage are available,
// if they are it uses them to make a beverage and sets remaining
// ingredient quantities correctly
func makeBeverage(inventory *Inventory, beverage string, recipe map[string]int) error {
	if err := inventory.TakeIngredients(beverage, recipe); err != nil {
		return err
	}

	// Make a beverage
	time.Sleep(PrepTimeInMillis * time.Millisecond)

	return nil
}

// Runs a completely independent outlet
// - reads ingredients of a beverage from 'recipes',
// - listens for new orders from 'orders' channel
// - singals completion using 'wg' waitgroup
// - 'index' is a unique ID for the outlet
func outlet(
	inventory *Inventory,
	index int,
	orders <-chan string,
	wg *sync.WaitGroup,
) {
	// Ensure waitgroup is notified after outlet shuts down
	defer wg.Done()

	// Start listeting for new beverage orders in the orders channel
	for beverage := range orders {
		debug(
			"Received order for beverage '%s' in outlet #%d",
			beverage, index,
		)

		var err error
		recipe, ok := inventory.recipes[beverage]
		if !ok {
			err = fmt.Errorf("invalid beverage name %s", beverage)
		} else {
			err = makeBeverage(inventory, beverage, recipe)
		}

		if err != nil {
			// If making the beverage failed, print the error appropriately
			fmt.Printf("%s cannot be prepared because %v\n", beverage, err)
		} else {
			// If making the beverage succeeded, print that it did
			fmt.Printf("%s is prepared\n", beverage)
		}

	}

	debug("Outlet #%d is shutting down", index)
}

// Primary entry point to start a complete coffee machine.
// Sets up required configuration and starts initializes outlets correctly
// - takes the path to a configuration file as an argument
// - returns:
//   - a chan that the caller can use to place orders
//   - a wait group that caller can use to wait for all outlets to complete
//   - an error to indicate any errors in starting the coffee machine
func StartMachine(configPath string) (chan<- string, *sync.WaitGroup, error) {
	// Read configuration from specified file path
	config, err := LoadConfig(configPath)
	if err != nil {
		return nil, nil, err
	}

	// Fill inventory with available ingredients and their quantities
	inventory := newInventory(config)

	// Create a channel to receive orders for beverages
	orders := make(chan string, 0)

	// Create a wait group that can wait for all concurrent
	// outlets to complete making all queued beverage orders
	// & then gracefully shutdown the coffee machine
	outletsWG := &sync.WaitGroup{}
	outletsWG.Add(config.Machine.Outlets.Count)

	// Create concurrent outlets, using count from configuration
	for index := 0; index < config.Machine.Outlets.Count; index++ {
		debug("Outlet #%d is starting", index)
		go outlet(inventory, index, orders, outletsWG)
	}

	return orders, outletsWG, nil
}
