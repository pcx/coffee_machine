package coffee_machine_test

import (
	"testing"

	machine "pcx.io/coffee_machine"
)

const (
	configPath = "./test_config.json"
)

func TestStartMachineCase1(t *testing.T) {
	orders, closer, err := machine.StartMachine(configPath)
	if err != nil {
		t.Errorf("Loading config failed with error: %v", err)
		return
	}

	orders <- "hot_tea"
	orders <- "hot_coffee"
	orders <- "green_tea"
	orders <- "black_tea"

	close(orders)
	closer.Wait()
}

func TestStartMachineCase2(t *testing.T) {
	orders, closer, err := machine.StartMachine(configPath)
	if err != nil {
		t.Errorf("Loading config failed with error: %v", err)
		return
	}

	orders <- "hot_tea"
	orders <- "black_tea"
	orders <- "green_tea"
	orders <- "hot_coffee"

	close(orders)
	closer.Wait()
}

func TestStartMachineCase3(t *testing.T) {
	orders, closer, err := machine.StartMachine(configPath)
	if err != nil {
		t.Errorf("Loading config failed with error: %v", err)
		return
	}

	orders <- "hot_coffee"
	orders <- "black_tea"
	orders <- "green_tea"
	orders <- "hot_tea"

	close(orders)
	closer.Wait()
}
