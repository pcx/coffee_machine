package coffee_machine

import (
	"encoding/json"
	"io/ioutil"
)

// Used to statically type the input json
type Config struct {
	Machine MachineConfig `json:"machine"`
}

type MachineConfig struct {
	Outlets            OutletsConfig             `json:"outlets"`
	TotalItemsQuantity map[string]int            `json:"total_items_quantity"`
	Beverages          map[string]map[string]int `json:"beverages"`
}

type OutletsConfig struct {
	Count int `json:"count_n"`
}

// Deserializes the input json available at `configPath` on the filesystem
// into a statically typed `Config` struct
func LoadConfig(configPath string) (*Config, error) {
	debug("***********************************")
	debug("** WELCOME TO THE COFFEE MACHINE **")
	debug("***********************************")

	debug("Loading config from file: %s\n", configPath)

	configBytes, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	config := &Config{}
	if err := json.Unmarshal(configBytes, config); err != nil {
		return nil, err
	}

	return config, err
}
