# Coffee Machine
## Have a cup mate!

### Running integration tests

The tests for the machine are present in `machine_test.go`. To run these tests you need to have the `go` executable on your `PATH`. This implementation is tested to work with `go` version `1.14.5`. Here are the commands you can use:

```bash
# Specifically run the machine-related integration tests
$ go test -v machine_test.go

# Run all tests
$ go test -v ./...

# Run tests with DEBUG logs enabled
$ DEBUG=true go test -v ./...
```

### Using a custom input JSON

The input JSON used by default is the 'Input Test JSON' that is provided in the problem statement. It has been added to this repository as the file `test_config.json` for ease of use. To use a custom input JSON, change the varibale `configPath` in `machine_test.go` to path of the file you want to use.
